# coding=utf-8

import re
import glob
import random
import sys
# from IPython.core.debugger import Tracer
# from nltk import tokenize
# import nltk
# nltk.download()

search_pattern_str = '\s섰|\s서 있' # sys.argv[1]
num_of_sentences_1 = 500#int(sys.argv[1])
num_of_sentences_2 = 1000#int(sys.argv[2])
print_match = 0

def print_to_file(result, length):
    if len(result) < length:
        print("찾은 문장의 갯수가 %d개입니다. 얻고자 하는 %d개의 sentence가 없으므로 프로그램이 종료됩니다." % ( len(result), length ) )
        exit(0)

    result = random.sample(result, length)

    fout = open('결과-'+str(length)+'.out', 'w', encoding='utf8')

    for s in result:
        fout.write(s)

    fout.close()

tag_pattern = re.compile('<[^>]*>')
search_pattern = re.compile(search_pattern_str)

result = []
for file in glob.glob("*.txt"):
    # puts "#{image}"
    print(file)
    try:
        # print("Try to open file with utf-16-le encoding")
        f = open(file, "r", encoding='utf-16-le')
        line = f.readlines()
        f.close()
        f = open(file, "r", encoding='utf-16-le')
    except:
        # print("Try to open file with euc-kr encoding")
        f = open(file, "r", encoding='cp949')
        line = f.readlines()
        f.close()
        f = open(file, "r", encoding='cp949')


    while True:
        line = f.readline()
        if not line: break
        line = re.sub(tag_pattern, '', line)
        # sentences = tokenize.sent_tokenize(paragraph)
        sentences = re.split(r' *[\.\?!][\'"\)\]]* *', line)
        # paragraph.split('.')
        # print(sentences)
        for s in sentences:
            s = s.strip()
            if s:
                # print(s)
                if search_pattern.search(s):
                    if print_match:
                        print(s)

                    result.append(s+"\n")
            #   fout.write(

    f.close()

print( "결과 갯수: ", len(result) )
print_to_file(result, num_of_sentences_1)
print_to_file(result, num_of_sentences_2)

