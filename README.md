# Query Sentences

My friend who major Korean Phonology was looking for "sentence query program" which can handle some conditions below.

1. Query multiple files at once.
2. These files could have multiple character encodings.
3. These files could have xml tags.
4. Sentences are seperated by '.', '!' or '?'.
5. Can choose number of sentences among the matched sentences.

Requirement 1, 4, 5 are now completed.

## How to Run

```
python query-sentences.py
```
